# A collection of `.gitlab-ci.yml` templates and includes for Security Products
[![build status](https://gitlab.com/gitlab-org/security-products/ci-templates/badges/master/build.svg)](https://gitlab.com/gitlab-org/security-products/ci-templates/commits/master) [![coverage report](https://gitlab.com/gitlab-org/security-products/ci-templates/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/ci-templates/commits/master)

This is Security Products collection of [`.gitlab-ci.yml`][ci-docs] file templates,
to be used in conjunction with [GitLab CI][gl-ci].

For more information about how `.gitlab-ci.yml` files work, and how to use them,
please read the [documentation on GitLab CI][ci-docs]. Please keep in mind that
these templates might need editing to suit your setup, and should be considered
guideline.

[ci-docs]: http://docs.gitlab.com/ce/ci/
[gl-ci]: https://about.gitlab.com/gitlab-ci/


## Folder structure

The `includes` folder holds production ready yaml files to be included in a main `.gitlab-ci.yml` template.
See [include feature documentation](https://docs.gitlab.com/ee/ci/yaml/#include).

The `includes-dev` folder holds yaml files for development process and tools of the Security Product team.
See [include feature documentation](https://docs.gitlab.com/ee/ci/yaml/#include).

## Contributing guidelines

Please see the [contribution guidelines](CONTRIBUTING.md)
